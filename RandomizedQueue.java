import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
   /** The queue */
   private Item[] queue;
   /** The size of the queue */
   private int N;

   /** Construct an empty randomized queue */
   public RandomizedQueue() {
      queue = (Item[]) new Object[2];
      N = 0;
   }

   /** Is the queue empty? */
   public boolean isEmpty() {
      return N == 0;
   }

   /** Return the number of items on the queue */
   public int size() {
      return N;
   }

   /** Add the item */
   public void enqueue(Item item) {
      if (item == null)
         throw new NullPointerException();
      /* Double the size of the array if necessary */
      if (N == queue.length)
         resize(2 * queue.length);
      queue[N++] = item;
   }

   /** Delete and return a random item */
   public Item dequeue() {
      if (isEmpty())
         throw new NoSuchElementException("Empty queue");
      int r = StdRandom.uniform(N);
      Item item = queue[r];
      /* Avoid loitering */
      queue[r] = null;
      /* Move the null to the last position, so that there are no blanks in the
         middle of the queue */
      swap(queue, r, N - 1);
      N -= 1;
      /* Shrink the size of the array if necessary */
      if (N > 0 && N == queue.length / 4)
         resize(queue.length / 2);
      return item;
   }

   /** Return (but do not delete) a random item */
   public Item sample() {
      if (isEmpty())
         throw new NoSuchElementException("Empty queue");
      int r = StdRandom.uniform(N);
      return queue[r];
   }

   /** Resize the underlying array holding the elements. It is assumed that
       the new capacity is enough to fit the currently stored elements */
   private void resize(int capacity) {
      Item[] temp = (Item[]) new Object[capacity];
      for (int i = 0; i < N; i++)
         temp[i] = queue[i];
      queue = temp;
   }

   /** Generate a copy of the queue with a random order, using Knuth shuffling
       algorithm */
   private Item[] knuthShuffle() {
     Item[] copy = (Item[]) new Object[N];
     for (int i = 0; i < N; i++) {
        copy[i] = queue[i];
        int r = StdRandom.uniform(i + 1);
        swap(copy, i, r);
     }
     return copy;
   }

   /** Swaps the position of two elements in the queue */
   private void swap(Item[] array, int i, int r) {
      Item temp = array[i];
      array[i] = array[r];
      array[r] = temp;
   }

   /** Return an independent iterator over items in random order */
   public Iterator<Item> iterator() {
      return new RandomIterator();
   }

   /** Inner class to generate the Iterators */
   private class RandomIterator implements Iterator<Item> {
      private Item[] randomQueue;
      private int current;
      public RandomIterator() {
         randomQueue = knuthShuffle();
         current = N-1;
      }
      public boolean hasNext() {
         return current >= 0;
      }

      public void remove() {
         throw new UnsupportedOperationException();
      }

      public Item next() {
         if (!hasNext())
            throw new NoSuchElementException();
         return randomQueue[current--];
      }
   }
}
