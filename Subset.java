public class Subset {
   public static void main(String[] args) {
      if (args.length < 1)
         StdOut.println("Usage: java Subset <K>");
      else {
         int k = new Integer(args[0]).intValue();

         /* To store the input data */
         RandomizedQueue<String> q = new RandomizedQueue<String>();

         /* Read and store the inputs */
         while (!StdIn.isEmpty()) {
            q.enqueue(StdIn.readString());
         }

         if (q.size() >= k) {
            /* Print k in random order */
            for (int i = 0; i < k; i++)
               StdOut.println(q.dequeue());
         } else {
            StdOut.println("K = " + k + " greater than the number of data " + q.size());
         }
      }
   }
}
