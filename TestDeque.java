public class TestDeque {

   /** Tests the insertion at the front */
   private boolean addFirst() {
      boolean returnValue = false;
      String str = "";
      Deque<String> deck = new Deque<String>();

      /* Insert elements */
      for (int i = 0; i < 10; i++) {
         String item = String.valueOf(i);
         deck.addFirst(item);
      }
      /* extract elements */
      for (String item: deck)
         str = str + item + " ";

      String expectedStr = "9 8 7 6 5 4 3 2 1 0 ";
      StdOut.println("EXPECTED <" + expectedStr + ">");
      StdOut.println("GOT      <" + str + ">");
      if (str.equals(expectedStr)) {
         returnValue = true;
         StdOut.println("[OK]");
      }
      else
         StdOut.println("[ERROR]");

      return returnValue;
    }

   /** Test the insertion at the end */
   private boolean addLast() {
      boolean returnValue = false;
      String str = "";
      Deque<String> deck = new Deque<String>();
      /* Insert elements */
      for (int i = 0; i < 10; i++) {
         String item = String.valueOf(i);
         deck.addLast(item);
      }
      /* extract elements */
      for (String item: deck)
         str = str + item + " ";

      String expectedStr = "0 1 2 3 4 5 6 7 8 9 ";
      StdOut.println("EXPECTED <" + expectedStr + ">");
      StdOut.println("GOT      <" + str + ">");
      if (str.equals(expectedStr)) {
         returnValue = true;
         StdOut.println("[OK]");
      }
      else
         StdOut.println("[ERROR]");

      return returnValue;
   }

   /** Test the removal from the front */
   private boolean removeFirst() {
      boolean returnValue = true;
      String str = "";
      Deque<String> deck = new Deque<String>();

      /* Insert elements */
      for (int i = 0; i < 10; i++) {
         String item = String.valueOf(i);
         deck.addFirst(item);
      }
      if (deck.size() != 10) {
         returnValue = false;
         StdOut.println("EXPECTED <10>");
         StdOut.println("GOT      <" + deck.size() + ">");
         StdOut.println("[ERROR]");
      }
      else {
         /* extract elements */
         for (int i = 9; i >= 0; i--) {
            int tempInt = new Integer(deck.removeFirst()).intValue();
            if (i != tempInt) {
               StdOut.println("EXPECTED <" + i + ">");
               StdOut.println("GOT      <" + tempInt + ">");
               StdOut.println("[ERROR]");
               returnValue = false;
               break;
            }
         }
      }
      if (returnValue && !deck.isEmpty()) {
         returnValue = false;
         StdOut.println("[ERROR] The deque should be empty");
      }

      if (returnValue)
         StdOut.println("[OK] test removeFirst()");
      return returnValue;
   }

   /** Test the removal from the front */
   private boolean removeLast() {
      boolean returnValue = true;
      String str = "";
      Deque<String> deck = new Deque<String>();

      /* Insert elements */
      for (int i = 0; i < 10; i++) {
         String item = String.valueOf(i);
         deck.addFirst(item);
      }
      if (deck.size() != 10) {
         returnValue = false;
         StdOut.println("EXPECTED <10>");
         StdOut.println("GOT      <" + deck.size() + ">");
         StdOut.println("[ERROR]");
      }
      else {
         /* extract elements */
         for (int i = 0; i < 10; i++) {
            int tempInt = new Integer(deck.removeLast()).intValue();
            if (i != tempInt) {
               StdOut.println("EXPECTED <" + i + ">");
               StdOut.println("GOT      <" + tempInt + ">");
               StdOut.println("[ERROR]");
               returnValue = false;
               break;
            }
         }
      }
      if (returnValue && !deck.isEmpty()) {
         returnValue = false;
         StdOut.println("[ERROR] The deque should be empty");
      }

      if (returnValue)
         StdOut.println("[OK] test removeLast()");
      return returnValue;
   }

   /** Test the tricky case of the Deque being non empty, then getting empty and
       becoming non empty again */
   private boolean nonEmptyToEmptyToNonEmpty() {
      boolean returnValue = true;
      Deque<String> deck = new Deque<String>();

      /* Add a couple of elements */
      deck.addFirst("hola");
      deck.addFirst("adios");
      /* check it has the right size */
      if (deck.size() != 2) {
         returnValue = false;
         StdOut.println("[ERROR] Wrong size, should be 2 and is " + deck.size());
      }
      
      if (returnValue) {
         /* empty the deque */
         deck.removeFirst();
         deck.removeFirst();

         if (!deck.isEmpty()) {
            returnValue = false;
            StdOut.println("[ERROR] Deque should be emtpy");
         }
      }

      if (returnValue) {
         /* Add a couple of elements */
         deck.addFirst("hola");
         deck.addFirst("adios");

         if (deck.size() != 2) {
            returnValue = false;
            StdOut.println("[ERROR] Wrong size, should be 2 and is " + deck.size());
         }
      }

      if (returnValue)
         StdOut.println("[OK] Testing non-empty > empty > non-empty transition");
      return returnValue;
   }

   private boolean nestedForeachLoops() {
      boolean returnValue = true;

      Deque<String> deck1 = new Deque<String>();
      Deque<String> deck2 = new Deque<String>();

      String[] results = new String[4];

      /* add elements to both */
      deck1.addLast("hola");
      deck1.addLast("adios");
      
      deck2.addLast("Pepe");
      deck2.addLast("Maria");

      /* try the nested foreach loops */
      int index = 0;
      for (String str1: deck1) {
         for (String str2: deck2) {
            results[index++] = str1 + " " + str2;
         }
      }

      /* Check the results */
      String[] check = {"hola Pepe", "hola Maria", "adios Pepe", "adios Maria"};
      for (int i = 0; i < 4; i++) {
         if (!check[i].equals(results[i])){
            returnValue = false;
            StdOut.println("EXPECTED <" + check[i] + ">");
            StdOut.println("GOT      <" + results[i] + ">");
            StdOut.println("[ERROR]");
            break;
         }
      }
      
      if (returnValue)
         StdOut.println("[OK] Nested `foreach` loops working right");
      return returnValue;
   }

   /* To run the tests */
   public static void main(String[] args) {
      TestDeque test = new TestDeque();
   
      StdOut.println("-------");
      test.addFirst();
      StdOut.println("-------");
      test.addLast();
      StdOut.println("-------");
      test.removeFirst();
      StdOut.println("-------");
      test.removeLast();
      StdOut.println("-------");
      test.nonEmptyToEmptyToNonEmpty();
      StdOut.println("-------");
      test.nestedForeachLoops();
   }
}
