import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {
   /* Size of the deque */
   private int N;
   /* Front of the deque */
   private BidirectionalNode first;
   /* Back of the deque */
   private BidirectionalNode last;

   /** Construct an empty deque */
   public Deque() {
      first = null;
      last = null;
      N = 0;
   }

   /** Is the deque empty? */
   public boolean isEmpty() {
      return N == 0;
   }

   /** Return the number of items on the deque */
   public int size() {
      return N;
   }

   /** Insert the item at the front */
   public void addFirst(Item item) {
      if (item == null)
         throw new NullPointerException();
      BidirectionalNode oldFirst = first;
      first = new BidirectionalNode();
      first.item = item;
      first.next = oldFirst;
      first.previous = null;
      /* The deque was empty */
      if (isEmpty())
         last = first;
      else
         oldFirst.previous = first;
      N += 1;
   }

   /** Insert the item at the end */
   public void addLast(Item item) {
      if (item == null)
         throw new NullPointerException();
      BidirectionalNode oldLast = last;
      last = new BidirectionalNode();
      last.item = item;
      last.next = null;
      last.previous = oldLast;
      /* The deque was empty */
      if (isEmpty())
         first = last;
      else
         oldLast.next = last;
      N += 1;
   }

   /** Delete and return the item at the front */
   public Item removeFirst() {
      if (isEmpty())
         throw new NoSuchElementException("Empty deque");
      Item item = first.item;
      first = first.next;
      N -= 1;
      if (isEmpty())
         last = null;
      else
         first.previous = null;
      return item;
   }

   /** Delete and return the item at the end */
   public Item removeLast() {
      if (isEmpty())
         throw new NoSuchElementException("Empty deque");
      Item item  = last.item;
      last = last.previous;
      N -= 1;
      if (isEmpty())
         first = null;
      else
         last.next = null;

      return item;
   }

   /** Return an iterator over the items in order from front to end */
   public Iterator<Item> iterator() {
      return new ForwardDequeIterator();
   }

   /** Inner class to implement the bidirectionaly linked list */
   private class BidirectionalNode {
      private Item item;
      private BidirectionalNode next;
      private BidirectionalNode previous;
   }

   /** Inner class to implement the iterator */
   private class ForwardDequeIterator implements Iterator<Item> {
      private BidirectionalNode current = first;

      public boolean hasNext() {
         return current != null;
      }
      public void remove() {
         throw new UnsupportedOperationException();
      }

      public Item next() {
         if (!hasNext())
            throw new NoSuchElementException();
         Item item = current.item;
         current = current.next;
         return item;
      }
   }
}
