import java.util.Iterator;

public class TestRandomizedQueue {
   /** Create a random queue */
   private boolean enqueueDequeueElements() {
      boolean flag = true;

      RandomizedQueue<String> q = new RandomizedQueue<String>();

      for (int i = 0; i < 10; i++) 
         q.enqueue(String.valueOf(i));
      if (q.size() != 10) {
         StdOut.println("[ERROR] The size should be 10 and is " + q.size());
         flag = false;
      }
      if (flag) {
         /* dequeue the elements */
         int N = q.size();
         for (int i = 9; i >= 0; i--)
            StdOut.print(q.dequeue() + " ");
         StdOut.print("\b\n");
         if (!q.isEmpty()) {
            StdOut.println("[ERROR] The size after emptying should be 0, and is " + q.size());
            flag = false;
         }
      }
      if (flag) {
         /* enqueue more elements */
         for (int i = 0; i < 5; i++)
            q.enqueue(String.valueOf(i));
         if (q.size() != 5) {
            StdOut.println("[ERROR] The size should be 5 and is " + q.size());
            flag = false;
         }
      }
      if (flag)
         StdOut.println("[OK] Test adding and removing elements works");
      return flag;
   }

   /** Tests the sampling of a queue */
   private boolean sample() {
      boolean flag = true;

      RandomizedQueue<String> q = new RandomizedQueue<String>();

      for (int i = 0; i < 100; i++)
         q.enqueue(String.valueOf(i));
      
      /* sample 10 times the result */
      for (int i = 0; i < 10; i++)
         StdOut.print(q.sample() + " ");
      StdOut.println("\b");

      return flag;
   }

   /** Test the iterators */
   private boolean independentIterators() {
      boolean flag = true;
      RandomizedQueue<String> q = new RandomizedQueue<String>();

      for (int i = 0; i < 10; i++)
         q.enqueue(String.valueOf(i));

      /* get two iterators and compare them */
      Iterator<String> it1 = q.iterator();
      Iterator<String> it2 = q.iterator();

      for (int i = 0; i < 10; i++)
         StdOut.println(it1.next() + " " + it2.next());

      return flag;
   }

   public static void main(String[] args) {
      TestRandomizedQueue test = new TestRandomizedQueue();

      StdOut.println("-------");
      test.enqueueDequeueElements();
      StdOut.println("-------");
      test.sample();
      StdOut.println("-------");
      test.independentIterators();
   }
}
